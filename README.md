# rke2-cis-ansible
Ansible playbook to setup a single node RKE2 Kubernetes cluster in CIS mode with SELINUX enforcement enabled.

```bash
               ,        ,  _______________________________
   ,-----------|'------'|  |                             |
  /.           '-'    |-'  |_____________________________|
 |/|             |    |    
   |   .________.'----'    _______________________________
   |  ||        |  ||      |                             |
   \__|'        \__|'      |_____________________________|

|‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾|
|________________________________________________________|

|‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾|
|________________________________________________________|
```
## `TLDR`
run this to deploy a hardened RKE2 cluster that:
* enforces SELINUX
* passes the *hardened* CIS-1.5 Kubernetes benchmark

## Instructions
```bash
git clone https://gitlab.com/gulla/rke2-cis-ansible
ansible-playbook ./rke2-cis-ansible/rke2-selinux-singlenode.yml
## wait until etcd/kube-api-server is up and running
sh ./rke2-cis-ansible/bin/patch-serviceaccounts-cis5.1.5.sh

```

## MANUAL STEP: CIS Control 5.1.5
For default service accounts in the built-in namespaces (kube-system, kube-public, kube-node-lease, and default), RKE2 does not automatically do this. You can manually update this field on these service accounts to pass the control.

To remediate this, run:

#### `./bin/patch-serviceaccounts-cis5.1.5.sh`
```bash
#!/bin/sh
# https://docs.rke2.io/security/hardening_guide/#control-515

kubectl patch serviceaccount default -n default -p '{"automountServiceAccountToken": false}'
kubectl patch serviceaccount default -n kube-public -p '{"automountServiceAccountToken": false}'
kubectl patch serviceaccount default -n kube-system -p '{"automountServiceAccountToken": false}'
kubectl patch serviceaccount default -n kube-node-lease -p '{"automountServiceAccountToken": false}'
```

## Resources
* https://docs.rke2.io/security/cis_self_assessment/
