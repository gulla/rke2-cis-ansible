#!/bin/sh
# https://docs.rke2.io/security/hardening_guide/#control-515

kubectl patch serviceaccount default -n default -p '{"automountServiceAccountToken": false}'
kubectl patch serviceaccount default -n kube-public -p '{"automountServiceAccountToken": false}'
kubectl patch serviceaccount default -n kube-system -p '{"automountServiceAccountToken": false}'
kubectl patch serviceaccount default -n kube-node-lease -p '{"automountServiceAccountToken": false}'
